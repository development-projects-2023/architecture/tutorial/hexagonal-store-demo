package com.hexagonal.architecture.application;

import com.hexagonal.architecture.domain.Order;
import com.hexagonal.architecture.infra.inputport.OrderInputPort;
import com.hexagonal.architecture.infra.outputport.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class OrderServiceImpl implements OrderInputPort {

    @Autowired
    EntityRepository entityRepository;

    @Override
    public Order save(String customerId, Double total) {

        Order order = Order.builder()
                .id(UUID.randomUUID().toString())
                .customerId(customerId)
                .total(total)
                .build();
        return entityRepository.save(order);
    }
}
