package com.hexagonal.architecture.application;

import com.hexagonal.architecture.domain.Customer;
import com.hexagonal.architecture.infra.inputport.CustomerInputPort;
import com.hexagonal.architecture.infra.outputport.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class CustomerServiceImpl implements CustomerInputPort {

    @Autowired
    EntityRepository entityRepository;

    @Override
    public Customer save(String name, String country) {
        Customer customer = Customer.builder()
                .id(UUID.randomUUID().toString())
                .name(name)
                .country(country)
                .build();
        return entityRepository.save(customer);
    }

    @Override
    public Customer findById(String customerId) {
        return entityRepository.findById(customerId, Customer.class);
    }

    @Override
    public List<Customer> findAll() {
        return entityRepository.findAll(Customer.class);
    }
}
