package com.hexagonal.architecture.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Order {

    private String id;
    private Double total;
    private String customerId;

}
