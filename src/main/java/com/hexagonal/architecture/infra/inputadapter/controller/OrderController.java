package com.hexagonal.architecture.infra.inputadapter.controller;

import com.hexagonal.architecture.domain.Order;
import com.hexagonal.architecture.infra.inputport.OrderInputPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/order")
public class OrderController {

    @Autowired
    OrderInputPort orderInputPort;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Order create(@RequestParam String customerId, @RequestParam Double total) {
        return orderInputPort.save(customerId, total);
    }
}
