package com.hexagonal.architecture.infra.inputadapter.controller;

import com.hexagonal.architecture.domain.Customer;
import com.hexagonal.architecture.infra.inputport.CustomerInputPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

    @Autowired
    CustomerInputPort customerInputPort;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Customer create(@RequestParam String name, @RequestParam String country) {
        return customerInputPort.save(name, country);
    }

    @GetMapping
    public Customer findById(@RequestParam String customerId) {
        return customerInputPort.findById(customerId);
    }

    @GetMapping("/findAll")
    public List<Customer> findAll() {
        return customerInputPort.findAll();
    }

}