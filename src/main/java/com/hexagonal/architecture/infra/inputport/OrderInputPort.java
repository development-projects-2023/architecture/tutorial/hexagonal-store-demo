package com.hexagonal.architecture.infra.inputport;

import com.hexagonal.architecture.domain.Order;

public interface OrderInputPort {

    public Order save(String customerId, Double total);
}
