package com.hexagonal.architecture.infra.inputport;

import com.hexagonal.architecture.domain.Customer;

import java.util.List;

public interface CustomerInputPort {

    Customer save(String name, String country);

    Customer findById(String customerId);

    List<Customer> findAll();


}
