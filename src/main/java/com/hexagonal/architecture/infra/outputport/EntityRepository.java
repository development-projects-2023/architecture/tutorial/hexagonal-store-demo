package com.hexagonal.architecture.infra.outputport;

import java.util.List;

public interface EntityRepository {

    <T> T save(T entity);

    <T> T findById(String id, Class<T> entityClass);

    <T> List<T> findAll(Class<T> entityClass);
}
