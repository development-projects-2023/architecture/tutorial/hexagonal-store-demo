# Hexagonal-Architecture
Proyecto que implementa una arquitectura hexagonal base.

## Authors
- [@yorsh](https://github.com/dev-yorsh)

## Appendix

Este repositorio consta de un proyecto en la cual tenemos lo siguiente:
**domain**: Contiene los clases modelo.
**output-port**: Contiene la interfaz de salida que es un repository.
**output-adapter**: Contiene la implementacion del **output-port**.
**input-port**: Contiene la interfaz que se comunicara con el **input-adapter**.
**input-adapter**: Contiene el controller que sera la entrada del usuario.
**application**: Contiene los servicios(casos de uso) del negocio.

## Screenshots

![App Screenshot](https://github.com/dev-yorsh/repository-images/blob/main/hexagonal-architecture/hexagonal-diagram-base.png?raw=true)
